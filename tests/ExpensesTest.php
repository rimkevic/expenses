<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExpensesTest extends TestCase
{
    /**
     * A get all expenses.
     *
     * /api/v1/expenses [GET]
     */
    public function testReturnAllExpenses()
    {
        $this->get("api/v1/expenses", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(

                [
                    'expense',
                    'description',
                    'created_at',
                    'updated_at',
                ]

        );
    }

    /**
     * A get exact exxpense.
     *
     * /api/v1/expenses/id [GET]
     */
    public function testReturnExpense()
    {
        $this->get("api/v1/expenses/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(

                [
                    'expense',
                    'description',
                    'created_at',
                    'updated_at',
                ]

        );
    }

    /**
     * /api/v1/expenses [POST]
     */
    public function testCreateExpense(){

        $parameters = [
            'expense' => 22,
            'description' => 'Test expense',
        ];

        $this->post("api/v1/expenses", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(

                [
                    'expense',
                    'description',
                ]

        );

    }
    /**
     * /api/v1/expenses/id [PATCH]
     */
    public function testUpdateExpense(){

        $parameters = [
            'expense' => 222,
            'description' => 'Test update',
        ];

        $this->patch("api/v1/expenses/1", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
                [
                    'expense',
                    'description',
                ]
        );
    }

    /**
     * /api/v1/expenses/id [DELETE]
     */
    public function testDeleteExpense(){
        $this->delete("api/v1/expenses/1", [], []);
        $this->seeStatusCode(200);
	}
}

