<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group([
    'prefix' => 'api/v1',
], function () use ($router){
    $router->get('/expenses', 'ExpensesController@index'); //get all expenses
    $router->get('/expenses/{id}', 'ExpensesController@show');//get one expense
    $router->post('/expenses', 'ExpensesController@store');//create new expense
    $router->patch('/expenses/{id}', 'ExpensesController@update');//update new expense
    $router->delete('/expenses/{id}', 'ExpensesController@destroy');//delete expense
});
