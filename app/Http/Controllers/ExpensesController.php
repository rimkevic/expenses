<?php
namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class ExpensesController extends Controller {

    /** @var Expense */
    protected $expense;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Expense $expense)
    {
            $this->expense = $expense;
    }

    /**
        * Display a listing of the resource.
        *
        * @return Response
        */
    public function index()
    {
        $expenses = $this->expense->all();

        return $expenses;
    }

    /**
     * Show specific expens
     *
     * @param int $id
     * @return Response

        */
    public function show($id)
    {
            $expense = $this->expense->find($id);

            if (empty($expense)) {
                    return "No expenses found.";
            }
            return $expense;
    }

    /**
        * Store a newly created resource in storage.
        *
        * @return Response
        */
    public function store(Request $request)
    {
        // Validate if the input for each field is correct
        $this->validate($request, [
            'expense' => 'required|integer',
            'description' => 'required|string',
           ]);

        // Create expense
        $expense = $this->expense->create([
            'expense' => $request->input('expense'),
            'description' =>  $request->input('description'),
        ]);

        return $expense;

    }

    /**
        * Update the specified resource in storage.
        *
        * @param  int  $id
        * @return Response
        */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expense' => 'required|integer',
            'description' => 'required|string',
        ]);

        $expense = $this->expense->find($id);

        if (empty($expense)) {
            return "No expenses found.";
        }
        $expense->update([
            'expense' => $request->input('expense'),
            'description' =>  $request->input('description'),
        ]);

        return $expense;
    }

    /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return Response
        */
    public function destroy($id)
    {
        $expense = $this->expense->find($id);

        if (empty($expense)) {
            return "No expenses found.";
        }
        $expense->delete();

        return $expense;

    }

}

