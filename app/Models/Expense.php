<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
        /**
         * @var array
         */
        protected $fillable = [
                'expense',
                'description',
        ];
}
